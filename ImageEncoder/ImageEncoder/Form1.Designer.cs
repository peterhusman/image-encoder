﻿namespace ImageEncoder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.endelabel = new System.Windows.Forms.Label();
            this.enButton = new System.Windows.Forms.Button();
            this.deButton = new System.Windows.Forms.Button();
            this.openImg = new System.Windows.Forms.OpenFileDialog();
            this.message = new System.Windows.Forms.TextBox();
            this.engobutton = new System.Windows.Forms.Button();
            this.seed = new System.Windows.Forms.NumericUpDown();
            this.seedL = new System.Windows.Forms.Label();
            this.saveImg = new System.Windows.Forms.SaveFileDialog();
            this.degobutton = new System.Windows.Forms.Button();
            this.messageDe = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.seed)).BeginInit();
            this.SuspendLayout();
            // 
            // endelabel
            // 
            this.endelabel.AutoSize = true;
            this.endelabel.Location = new System.Drawing.Point(94, 26);
            this.endelabel.Name = "endelabel";
            this.endelabel.Size = new System.Drawing.Size(67, 13);
            this.endelabel.TabIndex = 0;
            this.endelabel.Tag = "MainMenuItem";
            this.endelabel.Text = "Choose one:";
            // 
            // enButton
            // 
            this.enButton.Location = new System.Drawing.Point(80, 42);
            this.enButton.Name = "enButton";
            this.enButton.Size = new System.Drawing.Size(108, 23);
            this.enButton.TabIndex = 1;
            this.enButton.Tag = "MainMenuItem";
            this.enButton.Text = "Encode an Image";
            this.enButton.UseVisualStyleBackColor = true;
            this.enButton.Click += new System.EventHandler(this.selButton_Click);
            // 
            // deButton
            // 
            this.deButton.Location = new System.Drawing.Point(80, 71);
            this.deButton.Name = "deButton";
            this.deButton.Size = new System.Drawing.Size(108, 23);
            this.deButton.TabIndex = 2;
            this.deButton.Tag = "MainMenuItem";
            this.deButton.Text = "Decode an Image";
            this.deButton.UseVisualStyleBackColor = true;
            this.deButton.Click += new System.EventHandler(this.selButton_Click);
            // 
            // openImg
            // 
            this.openImg.FileName = "Image";
            this.openImg.Filter = "Image Files (.png, .bmp, .jpg, .jpeg)|*.png;*.bmp;*.jpg;*.jpeg";
            this.openImg.FileOk += new System.ComponentModel.CancelEventHandler(this.openImg_FileOk);
            // 
            // message
            // 
            this.message.Enabled = false;
            this.message.Location = new System.Drawing.Point(70, 110);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(127, 20);
            this.message.TabIndex = 3;
            this.message.Text = "Type your message here";
            this.message.Visible = false;
            this.message.Click += new System.EventHandler(this.message_Click);
            // 
            // engobutton
            // 
            this.engobutton.Location = new System.Drawing.Point(70, 191);
            this.engobutton.Name = "engobutton";
            this.engobutton.Size = new System.Drawing.Size(75, 23);
            this.engobutton.TabIndex = 6;
            this.engobutton.Text = "Encode";
            this.engobutton.UseVisualStyleBackColor = true;
            this.engobutton.Visible = false;
            this.engobutton.Click += new System.EventHandler(this.engobutton_Click);
            // 
            // seed
            // 
            this.seed.Enabled = false;
            this.seed.Location = new System.Drawing.Point(70, 165);
            this.seed.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.seed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.seed.Name = "seed";
            this.seed.Size = new System.Drawing.Size(120, 20);
            this.seed.TabIndex = 8;
            this.seed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seed.Visible = false;
            // 
            // seedL
            // 
            this.seedL.AutoSize = true;
            this.seedL.Location = new System.Drawing.Point(70, 146);
            this.seedL.Name = "seedL";
            this.seedL.Size = new System.Drawing.Size(78, 13);
            this.seedL.TabIndex = 9;
            this.seedL.Text = "Input the seed:";
            this.seedL.Visible = false;
            // 
            // saveImg
            // 
            this.saveImg.Filter = "Image Files (.png) | *.png";
            this.saveImg.FileOk += new System.ComponentModel.CancelEventHandler(this.saveImg_FileOk);
            // 
            // degobutton
            // 
            this.degobutton.Enabled = false;
            this.degobutton.Location = new System.Drawing.Point(73, 190);
            this.degobutton.Name = "degobutton";
            this.degobutton.Size = new System.Drawing.Size(75, 23);
            this.degobutton.TabIndex = 10;
            this.degobutton.Text = "Decode";
            this.degobutton.UseVisualStyleBackColor = true;
            this.degobutton.Visible = false;
            this.degobutton.Click += new System.EventHandler(this.degobutton_Click);
            // 
            // messageDe
            // 
            this.messageDe.AutoSize = true;
            this.messageDe.Location = new System.Drawing.Point(73, 110);
            this.messageDe.Name = "messageDe";
            this.messageDe.Size = new System.Drawing.Size(0, 13);
            this.messageDe.TabIndex = 11;
            this.messageDe.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 237);
            this.Controls.Add(this.messageDe);
            this.Controls.Add(this.degobutton);
            this.Controls.Add(this.seedL);
            this.Controls.Add(this.seed);
            this.Controls.Add(this.engobutton);
            this.Controls.Add(this.message);
            this.Controls.Add(this.deButton);
            this.Controls.Add(this.enButton);
            this.Controls.Add(this.endelabel);
            this.Name = "Form1";
            this.Text = "Image Encoder";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.seed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label endelabel;
        private System.Windows.Forms.Button enButton;
        private System.Windows.Forms.Button deButton;
        private System.Windows.Forms.OpenFileDialog openImg;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.Button engobutton;
        private System.Windows.Forms.NumericUpDown seed;
        private System.Windows.Forms.Label seedL;
        private System.Windows.Forms.SaveFileDialog saveImg;
        private System.Windows.Forms.Button degobutton;
        private System.Windows.Forms.Label messageDe;
    }
}

