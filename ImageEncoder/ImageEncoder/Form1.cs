﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;

namespace ImageEncoder
{
    public partial class Form1 : Form
    {
        bool encode = true;
        byte[] encryptedMes;
        Bitmap image;
        byte A;
        byte R;
        byte G;
        byte B;
        int seedV;
        int pixxpos;
        int pixypos;
        int ofFourIndex;
        bool decoding = true;
        StringBuilder mesDe = new StringBuilder();

        //openImg filter: Image Files (.png) | *.png
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void selButton_Click(object sender, EventArgs e)
        {
            if (sender.Equals(deButton))
            {
                encode = false;
            }
            else
            {
                encode = true;
            }
            openImg.ShowDialog();

        }

        private void openImg_FileOk(object sender, CancelEventArgs e)
        {
            Bitmap loadedImage = null;
            try
            {
                loadedImage = (Bitmap)Image.FromFile(openImg.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wow. Just wow. I applaud you. If you are reading this, you are either a physcopath genius or a bumbling idiot who managed to attach a png file extension to a text file. Wow. WHO GOES INTO CONSOLE JUST TO CRASH A UTILITY PROGRAM? Goodbye.", "", MessageBoxButtons.OK);
                Application.Exit();
                return;
                //Process.GetCurrentProcess().Kill();
            }
            string path = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            if (!path.Contains(".png"))
            {
                for (int i = path.Length - 1; i > 0; i--)
                {
                    if (path[i] == '.')
                    {
                        break;
                    }
                    path.Remove(i, 1);
                }
                path += "png";
            }
            if (loadedImage.PixelFormat != System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            {
                
                Bitmap sameExactImageButIn32BitsBecauseGDIPlusSucks = new Bitmap(loadedImage.Width, loadedImage.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                for (int x = 0; x < loadedImage.Width; x++)
                {
                    for (int y = 0; y < loadedImage.Height; y++)
                    {
                        Color color = loadedImage.GetPixel(x, y);
                        sameExactImageButIn32BitsBecauseGDIPlusSucks.SetPixel(x, y, Color.FromArgb(color.A, color.R, color.G, color.B));
                    }
                }
                
                    sameExactImageButIn32BitsBecauseGDIPlusSucks.Save(path, System.Drawing.Imaging.ImageFormat.Png);
                    loadedImage = (Bitmap)Image.FromFile(path);
            }

            image = loadedImage;

            if (encode)
            {
                enButton.Enabled = false;
                deButton.Enabled = false;
                endelabel.Enabled = false;
                message.Enabled = true;
                message.Visible = true;
                engobutton.Enabled = true;
                engobutton.Visible = true;
                seed.Visible = true;
                seed.Enabled = true;
                seedL.Visible = true;
                message.Text = "Type your message here";
            }
            else
            {
                enButton.Enabled = false;
                deButton.Enabled = false;
                endelabel.Enabled = false;
                messageDe.Visible = true;
                degobutton.Enabled = true;
                degobutton.Visible = true;
                seed.Visible = true;
                seed.Enabled = true;
                seedL.Visible = true;
                seedL.Enabled = true;
                messageDe.Text = "";
                messageDe.Enabled = true;
            }

        }


        private void engobutton_Click(object sender, EventArgs e)
        {
            string messageText = message.Text;
            encryptedMes = Encoding.UTF8.GetBytes(Convert.ToBase64String(Encoding.UTF8.GetBytes(message.Text.Replace("_", ""))) + "_");
            List<byte> tempEncryptedMes = new List<byte>(encryptedMes);
            while (tempEncryptedMes.Count % 4 != 0)
            {
                tempEncryptedMes.Add(Encoding.UTF8.GetBytes("_")[0]);
            }
            encryptedMes = tempEncryptedMes.ToArray();

            seedV = (int)seed.Value;
            foreach (byte b in encryptedMes)
            {
                ofFourIndex++;
                pixxpos = seedV;
                while (pixxpos >= image.Width)
                {
                    pixxpos -= image.Width;
                }
                pixypos = seedV / image.Width;

                if (ofFourIndex == 1)
                {
                    A = b;
                }
                else if (ofFourIndex == 2)
                {
                    R = b;
                }
                else if (ofFourIndex == 3)
                {
                    G = b;
                }
                else if (ofFourIndex == 4)
                {
                    B = b;
                    if (pixypos >= image.Height)
                    {
                        MessageBox.Show("Warning! Your message will not fit in the image given the current settings. Once you press OK, the program will close. Restart it. Either select a bigger image, select a smaller seed, or shorten your message.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Close();
                        break;
                    }
                    image.SetPixel(pixxpos, pixypos, Color.FromArgb(A, R, G, B));
                    ofFourIndex = 0;
                    seedV = (int)Math.Round(seedV * (decimal)1.5);
                }

            }
            saveImg.ShowDialog();
        }




        private void degobutton_Click(object sender, EventArgs e)
        {
            seedV = (int)seed.Value;
            byte[] tempByteArray;
            while (decoding)
            {
                pixxpos = seedV;
                while (pixxpos >= image.Width)
                {
                    pixxpos -= image.Width;
                }
                pixypos = seedV / image.Width;
                ofFourIndex++;
                if (pixypos > image.Height)
                {
                    MessageBox.Show("Warning! This image was either improperly encrypted or was not encrypted at all. You also may have chosen the wrong seed. When you press OK, the program will close. Restart it and select a valid image and the correct seed for that image.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Close();
                    break;
                }
                A = image.GetPixel(pixxpos, pixypos).A;
                R = image.GetPixel(pixxpos, pixypos).R;
                G = image.GetPixel(pixxpos, pixypos).G;
                B = image.GetPixel(pixxpos, pixypos).B;
                tempByteArray = new byte[] { A, R, G, B };
                mesDe.Append(Encoding.UTF8.GetString(tempByteArray));
                if (mesDe.ToString().Contains('_'))
                {
                    int index = 0;
                    foreach (char c in mesDe.ToString().ToArray())
                    {

                        if (c == '_')
                        {

                            mesDe.Remove(index, mesDe.Length - index);
                            
                            break;


                        }
                        index++;
                    }
                    try
                    {
                        string mes = Encoding.UTF8.GetString(Convert.FromBase64String(mesDe.ToString()));
                        
                        messageDe.Text = "The message is: " + mes;
                        if (mes == "")
                        {
                            messageDe.Text = "This image was encoded with no message.";
                        }
                        decoding = false;
                        degobutton.Enabled = false;
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString());
                        MessageBox.Show("Whoops! Something happened that probably shouldn't have. Press OK to get more details.", "Whoops!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                seedV = (int)Math.Round(seedV * (decimal)1.5);

            }
        }

        private void saveImg_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                image.Save(saveImg.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Congratulations! You found the absolute stupidest bug ever. It took us forever to find this. When you press OK, the program will close. Restart it, and next time, don't try to override the image that this program is working with. Basically, don't override the base image with the encoded image.", "Congratulations!", MessageBoxButtons.OK, MessageBoxIcon.None);
                Close();
            }
            foreach (Control control in Controls)
            {
                if (control.Tag != null && control.Tag.ToString() == "MainMenuItem")
                {
                    control.Enabled = true;
                }
                else
                {
                    control.Visible = false;
                    control.Enabled = false;
                }
            }
        }

        private void message_Click(object sender, EventArgs e)
        {
            message.Text = "";
        }


        //Other Encrypt
        //using (var rijndael = InitSymmetric(Rijndael.Create(), "TestPassword", 256))
        //{
        //    var text = "Some text to encrypt";
        //    var bytes = Encoding.UTF8.GetBytes(text);

        //    var encryptedBytes = Transform(bytes, rijndael.CreateEncryptor);
        //    var decryptedBytes = Transform(encryptedBytes, rijndael.CreateDecryptor);

        //    var decryptedText = Encoding.UTF8.GetString(decryptedBytes);
        //    Debug.Assert(text == decryptedText);
        //}
    }
}
